javascript-image_alternate_text
===============================

Returns the alternate text of an image.


* [LIVE DEMO](http://embed.plnkr.co/Yb51WW/)

